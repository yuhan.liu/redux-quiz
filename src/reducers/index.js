import {combineReducers} from "redux";
import notes from "../home/reducers/notes";
import noteDetail from "../note/reducers/noteDetail";

const reducers = combineReducers({
  notes,
  noteDetail
});
export default reducers;
