import React, {Component} from 'react';
import {connect} from 'react-redux';
import getNoteById from "../actions/getNoteById";
import NoteCatalog from "./NoteCatalog";
import deleteNote from "../actions/deleteNote";
import {Redirect} from "react-router";
import {Link} from "react-router-dom";

function mapStateToProps({noteDetail}) {
    return {
        detail: noteDetail.detail,
        isExist: noteDetail.isExist
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getDetail: (id) => dispatch(getNoteById(id)),
        deleteNote: (id) => dispatch(deleteNote(id))
    }
}


class NoteDetail extends Component {
    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {
        this.props.getDetail(this.props.match.params.id);
    }

    render() {
        if (this.props.isExist) {
            return (
                <div>
                    <section>
                        <NoteCatalog/>
                    </section>
                    <article>
                        <h1>{this.props.detail.title}</h1>
                        <p>{this.props.detail.description}</p>
                    </article>
                    <Link to="/">
                        <button onClick={this.deleteNote}>删除</button>
                    </Link>
                    <Link to="/">
                        <button>返回</button>
                    </Link>
                </div>
            )
        } else {
            return <Redirect to="/"/>
        }
    }

    deleteNote() {
        this.props.deleteNote(this.props.match.params.id);
    }

    returnHome() {
        this.render(

        )
    }
}

export default connect(
    mapStateToProps, mapDispatchToProps
)(NoteDetail);
